const cards = document.querySelectorAll('.memory-card');

let hasFlippeCard = false; 
//lock the cards
let lockBoard = false;
let firstCard, secondCard;

function flipCard() {
	if(lockBoard) return; //prevent any other card from being turned until the cards return to their initial state
	if(this === firstCard) return;// assess whether the second card clicked is the same as the first and return if positive:



	this.classList.add('flip'); //represents a card that has been selected


	if(!hasFlippeCard) { //if there is no card flipped
		hasFlippeCard = true;//saves the clicked letter.
		firstCard = this;
		return;
	}

	secondCard = this;
	


	checkForMatch();
}


function checkForMatch() {

let isMatch = firstCard.dataset.framework === secondCard.dataset.framework;
isMatch ? disableCards() : unflipCards();

}

function disableCards() {
	firstCard.removeEventListener('click', flipCard);
	secondCard.removeEventListener('click', flipCard);

	resetBoard();
}

function unflipCards() { //returns to its starting position.
	
	lockBoard = true;
	
	setTimeout(() => {
		firstCard.classList.remove('flip');
		secondCard.classList.remove('flip');

		resetBoard();

	}, 1500)
}


cards.forEach(card => card.addEventListener('click', flipCard));


//The firstCard and secondCart variables need to be reset after each round.

function resetBoard() {
	[hasFlippeCard, lockBoard] = [false, false];
	[firstCard, secondCart] = [null, null];
}

//The new method will be called by both functions disableCards() and unflipCards()


//shuffling
//iterate over the 12 cards and generate a random number between 0 and 11 then assign the property order
//Immediately Invoked Function Expression 
(function shuffle() {
	cards.forEach(card => {

		let randomPos = Math.floor(Math.random() * 12);
		card.style.order = randomPos;
	});

})();






